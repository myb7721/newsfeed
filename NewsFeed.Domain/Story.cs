﻿namespace NewsFeed.Domain
{
    /// <summary>
    /// News story domain model
    /// </summary>
    public class Story
    {
        public string Author { get; set; }
        public string Title { get; set; }
        public string Url {get; set;}
        public int Score { get; set; }
        public int NumberOfComments { get; set; }
    }
}
