﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsFeed.Domain
{
    public interface IStoryRepository
    {
        /// <summary>
        /// Fetches recent news stories.
        /// </summary>
        /// <param name="limit">The number of stories to fetch. Defaults to 50.</param>
        /// <returns>Recent news stories</returns>
        Task<IEnumerable<Story>> GetRecentStories(int limit = 50);
    }
}
