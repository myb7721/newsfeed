import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})
export class FetchDataComponent {
  public stories: Story[];
  public error: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Story[]>(baseUrl + 'api/NewsFeed/Stories')
      .subscribe(
        result => {
          this.stories = result;
        },
        error => {
          console.error(error);
          this.error = error.error.Message;
        }
      );
  }
}

interface Story {
  title: string;
  author: string;
  url: string;
  score: number;
  numberOfComments: number;
}
