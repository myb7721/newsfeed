﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NewsFeed.Domain;

namespace NewsFeed.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsFeedController : ControllerBase
    {
        private readonly IStoryRepository _repository;

        public NewsFeedController(IStoryRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Endpoint providing recent news stories.
        /// </summary>
        /// <param name="limit">The number of stories to fetch. Defaults to 50.</param>
        /// <returns>Recent news stories.</returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> Stories(int limit = 50)
        {
            try
            {
                return Ok(await _repository.GetRecentStories(limit));
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }
    }
}