﻿using NewsFeed.Domain;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using System.Linq;

namespace NewsFeed.Data
{
    /// <summary>
    /// Provides story data sourced from the HackerNews API
    /// </summary>
    public class HackerNewsStoryRepository : IStoryRepository
    {
        private static readonly HttpClient _client = new HttpClient();
        private static readonly DataContractJsonSerializer _serializer = new DataContractJsonSerializer(typeof(IEnumerable<int>));
        private const string _newStoriesUrl = @"https://hacker-news.firebaseio.com/v0/topstories.json";
        private const string _itemUrl = @"https://hacker-news.firebaseio.com/v0/item/";

        /// <summary>
        /// Fetches stories.
        /// </summary>
        /// <param name="limit">Number of stories to fetch.</param>
        /// <returns>Fetched stories</returns>
        public async Task<IEnumerable<Story>> GetRecentStories(int limit = 50)
        {
            IEnumerable<int> newStoryIds = await GetRecentStoryIds(limit);

            return await GetStoriesById(newStoryIds.Take(limit));
        }

        private async Task<IEnumerable<int>> GetRecentStoryIds(int limit = 50)
        {
            var response = await _client.GetAsync(_newStoriesUrl);
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<int>>(json);
        }

        private async Task<IEnumerable<Story>> GetStoriesById(IEnumerable<int> ids)
        {
            List<Story> stories = new List<Story>();

            List<Task<HttpResponseMessage>> responseTasks = new List<Task<HttpResponseMessage>>();
            foreach (var id in ids)
            {
                responseTasks.Add(_client.GetAsync($"{_itemUrl}{id}.json"));
            }

            await Task.WhenAll(responseTasks);

            foreach (var responseTask in responseTasks)
            {
                var response = await responseTask;
                var json = await response.Content.ReadAsStringAsync();
                var item = JsonConvert.DeserializeObject<HackerNewsItem>(json);

                var story = new Story()
                {
                    Author = item.By,
                    Title = item.Title,
                    Url = item.Url,
                    Score = item.Score,
                    NumberOfComments = item.Kids == null ? 0 : item.Kids.Count()
                };

                stories.Add(story);
            }

            return stories;
        }
    }
}
