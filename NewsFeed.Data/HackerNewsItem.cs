﻿using System.Collections.Generic;

namespace NewsFeed.Data
{
    /// <summary>
    /// Hacker news item model
    /// </summary>
    public class HackerNewsItem
    {
        public string By { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int Score { get; set; }
        public IEnumerable<int> Kids { get; set; }
    }
}
